from setuptools import find_packages, setup


version = '0.0.1a'


setup(
    name='pybracket',
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    version=version,
    license='BSD',
    description='A LISP interpreter',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    author='Kevin Etienne',
    author_email='etienne.kevin@gmail.com',
    url='https://gitlab.com/kevin42/pybracket',
)
